from bs4 import BeautifulSoup
import sqlite3
import re
import string

conn = sqlite3.connect("s1.db")

def query_1(q):
    res = conn.execute("SELECT * FROM entries WHERE word = '%s'" % q.lower())

    if len(res.fetchall()) > 0:
        return True

    return False


def query_2(q):

    qval = {'qstr' : q}
    sql_template = string.Template("""SELECT * FROM entries WHERE definition LIKE  '%$qstr%'""")

    sql = sql_template.substitute(qval)
    res = conn.execute(sql)

    if len(res.fetchall()) > 0:
        return True

    return False



res = query_1('aback')
if(res):
    print('found direct' )

res = query_2('cutdown')
if(res):
    print('found in description' )

