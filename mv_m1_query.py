from pymongo import MongoClient
import sqlite3
import string

conn = sqlite3.connect("./s1_v4.db")

# init logging
# # logging.basicConfig(filename='../../logs/mvalchemyscripts.log', level=logging.DEBUG)
# logging.debug("Logging Configured!")
client = MongoClient('10.10.10.10')

m1 = client.m1
class M1WordDefinition():

    word = ''

    pos = None
    definitions = None
    synonyms = None

    senses = None

    def __init__(self, term):

        self.pos = []
        self.definitions = []
        self.synonyms = []
        self.senses = []

        self.find_one_term(term)

    def find_one_term(self, term):

        self.word = term

        wdefs = m1.words.find({'term': term})

        for wdef in wdefs:

            if 'POS' in wdef:
                self.pos.append(str(wdef['POS']))

            if 'definitions' in wdef:
                for defs in wdef['definitions']:
                    self.definitions.append(defs)

            if 'relations' in wdef:
                for relations in wdef['relations']:
                    if relations['type'].find('SYNONYM') > 0:
                        self.synonyms.append(relations['relation'])

            if 'sense' in wdef:
                self.senses.append(wdef['sense'])


from bs4 import BeautifulSoup
import sqlite3
import re
import string


class S1WordDefinition:
    word = ""
    definition = None

def query_word_s1(q):
    res = conn.execute("SELECT * FROM entries WHERE word = '%s'" % q.lower())

    resultset = []
    for r in res:
        wd = S1WordDefinition()
        wd.word = r[0]
        wd.definition = r[2]
        resultset.append(wd)

    if len(resultset) > 0:
        return True, resultset

    return False, None


def query_in_description_s1(q):

    qval = {'qstr' : q}
    sql_template = string.Template("""SELECT * FROM entries WHERE definition LIKE  '%$qstr%'""")

    sql = sql_template.substitute(qval)
    res = conn.execute(sql)

    resultset = []
    for r in res:
        wd = S1WordDefinition()
        wd.word = r[0]
        wd.definition = r[2]
        resultset.append(wd)

    if len(resultset) > 0:
        return True, resultset

    return False, None

def loadwordsfromfile(wordfilename):

    keywords = []
    keys = open(wordfilename)
    for words in keys:
        keywords.append(words.strip('\n').strip('\r'))

    return keywords


def query_3(word):

    all = {}

    found, results = query_word_s1(word)
    if found:
        for w in results:
            for word in w.definition.split('|'):
                if len(word) > 2:
                    all[word] = word

    word = M1WordDefinition(word)
    if word:
        if word.synonyms:
            for s in word.synonyms:
                if len(s) > 0 and s is not ['Inf', 'Obj']:
                    w1 = s.replace('Wikisaurus:', '')
                    all[s] = w1

    return all.values()




if __name__ == '__main__':

    list_999 = loadwordsfromfile('999.txt')
    for each_word in list_999:
        print '[' + each_word + ']' + ' | '.join(query_3(each_word))

