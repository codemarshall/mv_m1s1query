import string
import sqlite3
import string
from pymongo import MongoClient
client = MongoClient('10.10.10.10')

m2 = client.m2
conn = sqlite3.connect("./s1_v4.db")

class S1WordDefinition:
    word = ""
    definition = None

def query_word_s1(q):
    res = conn.execute("SELECT * FROM entries WHERE word = '%s'" % q.lower())

    resultset = []
    for r in res:
        wd = S1WordDefinition()
        wd.word = r[0]
        wd.definition = r[2]
        resultset.append(wd)

    if len(resultset) > 0:
        return True, resultset

    return False, None

def query_word_s1_synonyms(q):

    all = {}
    found, results = query_word_s1(q)
    if found:
        for w in results:
            for word in w.definition.split('|'):
                if len(word) > 2:
                    all[word] = word

    return list(all)

class WordDefinition():

    word = ''

    pos = None
    definitions = None
    synonyms = None

    senses = None

    def __init__(self, term):

        self.pos = []
        self.definitions = []
        self.synonyms = []
        self.senses = []

        self.find_one_term(term)

    def find_one_term(self, term):

        self.word = term

        wdefs = m2.words.find({'term': term})

        for wdef in wdefs:

            if 'POS' in wdef:
                self.pos.append(str(wdef['POS']))

            if 'definitions' in wdef:
                for defs in wdef['definitions']:
                    self.definitions.append(defs)

            if 'relations' in wdef:
                for relations in wdef['relations']:
                    if relations['type'].find('SYNONYM') > -1:
                        self.synonyms.append(relations['relation'].replace('Wikisauraus:', ''))

            if 'sense' in wdef:
                self.senses.append(wdef['sense'])


def loadwordsfromfile(wordfilename):

    keywords = []
    keys = open(wordfilename)
    for words in keys:
        keywords.append(words.strip('\n').strip('\r'))

    return keywords

def rule1 (w1, w2):

    if (w1 in set(WordDefinition(w2).synonyms)) or (w2 in set(WordDefinition(w1).synonyms)):
        return 1

    return 0

def rule2(w1, w2):
    if len(set(WordDefinition(w1).synonyms).intersection(set(WordDefinition(w2).synonyms))) > 1:
        return 1

    return 0


if __name__ == '__main__':

    w1_999 = loadwordsfromfile('999w1.txt')
    w2_999 = loadwordsfromfile('999w2.txt')

    for idx in range(0, len(w1_999)):
        if len (WordDefinition(w1_999[idx]).synonyms) < 1 and len (WordDefinition(w2_999[idx]).synonyms) < 1:
            print w1_999[idx] + ',' + w2_999[idx] + ',NAN,NAN'
        else:
            print w1_999[idx] + ',' + w2_999[idx] + ',' + str(rule1(w1_999[idx], w2_999[idx])) + ',' + str(rule2(w1_999[idx], w2_999[idx]))

        # word = WordDefinition(each_word)
        #
        # print '============'
        # print each_word
        # print '============'
        #
        # print 'M2 SYNONYMS:' + ' | '.join(word.synonyms)


